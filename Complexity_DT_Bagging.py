import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

K = [2,4,6,8,10]
max_depth = [8,17,34,68]


k=10
Train_Error = []
Test_Error = []
for m in max_depth:
    csv_name = f'Bagging_DT_MaxDepth_{m}_{k}_Fold_Validation_Errors.csv'
    df = pd.read_csv(f'CSVs_for_Plotting/{csv_name}')

    # print(df)
    train =df.Train_Error
    # print(train.mean())
    test = df.Test_Error
    Train_Error.append(train.mean())
    Test_Error.append(test.mean())


print(Train_Error)
print(Test_Error)

# K = np.array(K)
# max_depth = np.array(max_depth)

df2 = pd.DataFrame({
                    "Max_Depth": max_depth,
                    "Train_Error": Train_Error,
                    "Test_Error": Test_Error})

csv_name = f'Bagging_DT_Complexity_Errors_{k}_Fold.csv'
df2.to_csv(f'CSV_COmplexity_with_K/{csv_name}')

exit(0)


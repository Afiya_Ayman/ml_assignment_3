#Imports
import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from  sklearn.model_selection import KFold
from sklearn.svm import LinearSVC
from sklearn.ensemble import BaggingClassifier, AdaBoostClassifier, RandomForestClassifier, GradientBoostingClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import stochastic_gradient
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score

#Headers of 35 attributes
headers = ["attribute_1", "attribute_2", "attribute_3", "attribute_4", "attribute_5","attribute_6", "attribute_7", "attribute_8", "attribute_9","attribute_10", "attribute_11", "attribute_12", "attribute_13", "attribute_14","attribute_15", "attribute_16", "attribute_17", "attribute_18",
           "attribute_19", "attribute_20", "attribute_21", "attribute_22", "attribute_23","attribute_24", "attribute_25", "attribute_26","attribute_27","attribute_28","attribute_29","attribute_30","attribute_31","attribute_32","attribute_33","attribute_34","class"]

# Read in the CSV file and convert "?" to NaN
df = pd.read_csv("ionosphere.data", header=None, names=headers)

# description = df.describe()
# description.to_csv("Data_Description.csv")

#Split the data into test lebels and a prediction lebel
X = np.array(df.drop(['class'], 1))
y = np.array(df['class'])

Number_of_Features = np.shape(X)[1]


#Split the sets into train and test set
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)



# K=[2,4,6,8,10]
k = 10
train_errors_by_k=[]
test_errors_by_k=[]

MAX_Depth = [8,17,34,68]
MAX_Depth = [1,2,3,4]
Number_of_Estimator = [10,15,20,25,30,35,40,45,50]


Train_Error = []
Test_Error = []
Complexity_Depth = []
Complexity_with_Degree = []
Accuracy = []

for d in range(2,30):
    #Test indics will come from stratified cross validation

    k_Fold = KFold(n_splits=k, random_state=36851234, shuffle=False)

    train_error=0
    test_error=0
    count=0

    individual_train_error=[]
    individual_test_error=[]
    iterations=[]

    #Repeate the testing and analyzing
    for train_index, test_index in k_Fold.split(X, y):
        #Separete train and test indics for X and y
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]



        ###Bagging
        # Algorithm 1 == Default classifier == decision tree
        # max_depth = 34
        # Classifier = BaggingClassifier(base_estimator=DecisionTreeClassifier(max_depth=max_depth), max_samples=0.5, max_features=0.5)


        ##Algorithm 2 == SVM
        # svm = LinearSVC(random_state=42)
        svm = SVC(probability=True, kernel='poly',degree=d,gamma='scale')
        Classifier = BaggingClassifier(base_estimator=svm, n_estimators=25, random_state=314)





        #Boosting
        ##Algorithm 1 == Default classifier == decision tree
        # max_depth = 2
        # Classifier = AdaBoostClassifier(base_estimator=DecisionTreeClassifier(max_depth=max_depth),n_estimators=4)

        ##Algorithm 2 == KNeighborsClassifier
        from sklearn import metrics

        # svc = SVC(probability=True, kernel='poly',degree=d,gamma='scale')
        # Classifier = AdaBoostClassifier(n_estimators=3, base_estimator=svc,learning_rate=0.1)


        Classifier.fit(X_train, y_train)

        train_error=train_error+(1-Classifier.score(X_train, y_train))
        test_error=test_error+(1-Classifier.score(X_test, y_test))
        count+=1
        iterations.append(count)
        individual_train_error.append(1-Classifier.score(X_train, y_train))
        individual_test_error.append(1 - Classifier.score(X_test, y_test))

    # # individual_train_error = individual_train_error.mean()
    # print(max_depth)
    # print(individual_train_error)
    # # individual_test_error = individual_test_error.mean()
    # print(individual_test_error)
    # print('\n\n')

    mean = sum(individual_train_error)/k
    Train_Error.append(mean)
    mean = sum(individual_test_error)/k
    Test_Error.append(mean)

    Complexity_with_Degree.append(d)

    ## Store the predicted values
    y_pred = Classifier.predict(X_test)

    # Calculate global accuracy
    accuracy = accuracy_score(y_test, y_pred)

    # print(f'\naccuracy with {k} fold = {accuracy}\n\n')
    accuracy = Classifier.score(X_test, y_test)
    accuracy = Classifier.score(X_test, y_test)
    print(f'\naccuracy with {k} fold = {accuracy}\n\n')
    Accuracy.append(accuracy)

print(Complexity_with_Degree)
print(Train_Error)
print(Test_Error)
print(Accuracy)

plt.plot(Accuracy)
plt.show()

exit()

df = pd.DataFrame({"Complexity_with_Degree":Complexity_with_Degree,
                   "Accuracy":Accuracy,
                    "Train_Error": Train_Error,
                    "Test_Error":Test_Error})

#Individual plots for folds
csv_name = f'Bagging_SVM_{k}_Fold_Different_Complexity.csv'
# csv_name = f'Boosting_DT_MaxDepth_{max_depth}_{k}_Fold_Validation_Errors.csv'
# csv_name = f'Boosting_SVM_{k}_Fold_Validation_Errors.csv'
df.to_csv(f'CSV_COmplexity_with_K/{csv_name}',index=False)





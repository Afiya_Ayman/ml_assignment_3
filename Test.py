import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

K = [2,4,6,8,10]
Models = ['Bagging_DT','Bagging_SVM', 'Boosting_DT','Boosting_SVM']

# for i in range(1,11):
#     print(f'Boosting_SVM_Test.append(df.Test_Error_{i}[3])')
#
# exit(0)

k = 2
Bagging_DT_Train = []
Bagging_SVM_Train = []
Boosting_DT_Train = []
Boosting_SVM_Train = []

Bagging_DT_Test = []
Bagging_SVM_Test = []
Boosting_DT_Test = []
Boosting_SVM_Test = []

csv_name = f'Train_Test_Errors_{k}_Fold_all_Algorithm.csv'
df=pd.read_csv(f'CSVs_for_Plotting/{csv_name}')

print(df.columns)
# print(df.Model_Name)
# exit(0)

# train_1 = df.Train_Error_1
# test =  df.Train_Error_2


Bagging_DT_Train.append(df.Train_Error_1[0])
Bagging_DT_Train.append(df.Train_Error_2[0])
# Bagging_DT_Train.append(df.Train_Error_3[0])
# Bagging_DT_Train.append(df.Train_Error_4[0])
# Bagging_DT_Train.append(df.Train_Error_5[0])
# Bagging_DT_Train.append(df.Train_Error_6[0])
# Bagging_DT_Train.append(df.Train_Error_7[0])
# Bagging_DT_Train.append(df.Train_Error_8[0])
# Bagging_DT_Train.append(df.Train_Error_9[0])
# Bagging_DT_Train.append(df.Train_Error_10[0])

Bagging_SVM_Train.append(df.Train_Error_1[1])
Bagging_SVM_Train.append(df.Train_Error_2[1])
# Bagging_SVM_Train.append(df.Train_Error_3[1])
# Bagging_SVM_Train.append(df.Train_Error_4[1])
# Bagging_SVM_Train.append(df.Train_Error_5[1])
# Bagging_SVM_Train.append(df.Train_Error_6[1])
# Bagging_SVM_Train.append(df.Train_Error_7[1])
# Bagging_SVM_Train.append(df.Train_Error_8[1])
# Bagging_SVM_Train.append(df.Train_Error_9[1])
# Bagging_SVM_Train.append(df.Train_Error_10[1])

Boosting_DT_Train.append(df.Train_Error_1[2])
Boosting_DT_Train.append(df.Train_Error_2[2])
# Boosting_DT_Train.append(df.Train_Error_3[2])
# Boosting_DT_Train.append(df.Train_Error_4[2])
# Boosting_DT_Train.append(df.Train_Error_5[2])
# Boosting_DT_Train.append(df.Train_Error_6[2])
# Boosting_DT_Train.append(df.Train_Error_7[2])
# Boosting_DT_Train.append(df.Train_Error_8[2])
# Boosting_DT_Train.append(df.Train_Error_9[2])
# Boosting_DT_Train.append(df.Train_Error_10[2])

Boosting_SVM_Train.append(df.Train_Error_1[3])
Boosting_SVM_Train.append(df.Train_Error_2[3])
# Boosting_SVM_Train.append(df.Train_Error_3[3])
# Boosting_SVM_Train.append(df.Train_Error_4[3])
# Boosting_SVM_Train.append(df.Train_Error_5[3])
# Boosting_SVM_Train.append(df.Train_Error_6[3])
# Boosting_SVM_Train.append(df.Train_Error_7[3])
# Boosting_SVM_Train.append(df.Train_Error_8[3])
# Boosting_SVM_Train.append(df.Train_Error_9[3])
# Boosting_SVM_Train.append(df.Train_Error_10[3])


Bagging_DT_Test.append(df.Test_Error_1[0])
Bagging_DT_Test.append(df.Test_Error_2[0])
# Bagging_DT_Test.append(df.Test_Error_3[0])
# Bagging_DT_Test.append(df.Test_Error_4[0])
# Bagging_DT_Test.append(df.Test_Error_5[0])
# Bagging_DT_Test.append(df.Test_Error_6[0])
# Bagging_DT_Test.append(df.Test_Error_7[0])
# Bagging_DT_Test.append(df.Test_Error_8[0])
# Bagging_DT_Test.append(df.Test_Error_9[0])
# Bagging_DT_Test.append(df.Test_Error_10[0])

Bagging_SVM_Test.append(df.Test_Error_1[1])
Bagging_SVM_Test.append(df.Test_Error_2[1])
# Bagging_SVM_Test.append(df.Test_Error_3[1])
# Bagging_SVM_Test.append(df.Test_Error_4[1])
# Bagging_SVM_Test.append(df.Test_Error_5[1])
# Bagging_SVM_Test.append(df.Test_Error_6[1])
# Bagging_SVM_Test.append(df.Test_Error_7[1])
# Bagging_SVM_Test.append(df.Test_Error_8[1])
# Bagging_SVM_Test.append(df.Test_Error_9[1])
# Bagging_SVM_Test.append(df.Test_Error_10[1])

Boosting_DT_Test.append(df.Test_Error_1[2])
Boosting_DT_Test.append(df.Test_Error_2[2])
# Boosting_DT_Test.append(df.Test_Error_3[2])
# Boosting_DT_Test.append(df.Test_Error_4[2])
# Boosting_DT_Test.append(df.Test_Error_5[2])
# Boosting_DT_Test.append(df.Test_Error_6[2])
# Boosting_DT_Test.append(df.Test_Error_7[2])
# Boosting_DT_Test.append(df.Test_Error_8[2])
# Boosting_DT_Test.append(df.Test_Error_9[2])
# Boosting_DT_Test.append(df.Test_Error_10[2])

Boosting_SVM_Test.append(df.Test_Error_1[3])
Boosting_SVM_Test.append(df.Test_Error_2[3])
# Boosting_SVM_Test.append(df.Test_Error_3[3])
# Boosting_SVM_Test.append(df.Test_Error_4[3])
# Boosting_SVM_Test.append(df.Test_Error_5[3])
# Boosting_SVM_Test.append(df.Test_Error_6[3])
# Boosting_SVM_Test.append(df.Test_Error_7[3])
# Boosting_SVM_Test.append(df.Test_Error_8[3])
# Boosting_SVM_Test.append(df.Test_Error_9[3])
# Boosting_SVM_Test.append(df.Test_Error_10[3])

print(Bagging_DT_Train)
print(Bagging_SVM_Train)
print(Boosting_DT_Train)
print(Boosting_SVM_Train)
print(Bagging_DT_Test)
print(Bagging_SVM_Test)
print(Boosting_DT_Test)
print(Boosting_SVM_Test)

df2 = pd.DataFrame({"Bagging_DT_Train":Bagging_DT_Train,
                    "Bagging_DT_Test":Bagging_DT_Test,

                    "Bagging_SVM_Train":Bagging_SVM_Train,
                    "Bagging_SVM_Test":Bagging_SVM_Test,

                    "Boosting_DT_Train":Boosting_DT_Train,
                    "Boosting_DT_Test":Boosting_DT_Test,

                    "Boosting_SVM_Train":Boosting_SVM_Train,
                    "Boosting_SVM_Test":Boosting_SVM_Test,
                    })

csv_name = f'Train_Test_Errors_{k}_Fold_all_Algorithm.csv'
df2.to_csv(f'PLot_Together/{csv_name}')

exit(0)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

K = [2,4,6,8,10]
Models = ['Bagging_DT','Bagging_SVM', 'Boosting_DT','Boosting_SVM']



k=2
Train_Error = []
Test_Error = []
Train_Error_Columns = []
Test_Error_Columns = []
# for i in range(k):
#     print(f'"Train_Error_{i+1}":Train_Error_{i+1},')
# for i in range(k):
#     print(f'"Test_Error_{i+1}":Test_Error_{i+1},')



Train_Error_1 = []
Train_Error_2 = []
Train_Error_3 = []
Train_Error_4 = []
Train_Error_5 = []
Train_Error_6 = []
Train_Error_7 = []
Train_Error_8 = []
Train_Error_9 = []
Train_Error_10 = []


Test_Error_1 = []
Test_Error_2 = []
Test_Error_3 = []
Test_Error_4 = []
Test_Error_5 = []
Test_Error_6 = []
Test_Error_7 = []
Test_Error_8 = []
Test_Error_9 = []
Test_Error_10 = []


for m in Models:
    csv_name = f'{m}_{k}_Fold_Validation_Errors.csv'
    df = pd.read_csv(f'CSVs_for_Plotting/{csv_name}')
    train =df.Train_Error
    print(train)
    test = df.Test_Error
    print(test)

    Train_Error_1.append(train[0])
    Train_Error_2.append(train[1])
    # Train_Error_3.append(train[2])
    # Train_Error_4.append(train[3])
    # Train_Error_5.append(train[4])
    # Train_Error_6.append(train[5])
    # Train_Error_7.append(train[6])
    # Train_Error_8.append(train[7])
    # Train_Error_9.append(train[8])
    # Train_Error_10.append(train[9])

    Test_Error_1.append(test[0])
    Test_Error_2.append(test[1])
    # Test_Error_3.append(test[2])
    # Test_Error_4.append(test[3])
    # Test_Error_5.append(test[4])
    # Test_Error_6.append(test[5])
    # Test_Error_7.append(test[6])
    # Test_Error_8.append(test[7])
    # Test_Error_9.append(test[8])
    # Test_Error_10.append(test[9])

    print(m)
    print(k)
    print(Train_Error)
    print(Test_Error)

df2 = pd.DataFrame({"Model_Name":Models,
                    "Train_Error_1":Train_Error_1,
                    "Train_Error_2":Train_Error_2,
                    # "Train_Error_3":Train_Error_3,
                    # "Train_Error_4":Train_Error_4,
                    # "Train_Error_5":Train_Error_5,
                    # "Train_Error_6":Train_Error_6,
                    # "Train_Error_7":Train_Error_7,
                    # "Train_Error_8":Train_Error_8,
                    # "Train_Error_9":Train_Error_9,
                    # "Train_Error_10":Train_Error_10,

                    "Test_Error_1":Test_Error_1,
                    "Test_Error_2":Test_Error_2
                    # "Test_Error_3":Test_Error_3,
                    # "Test_Error_4":Test_Error_4
                    # "Test_Error_5":Test_Error_5,
                    # "Test_Error_6":Test_Error_6
                    # "Test_Error_7":Test_Error_7,
                    # "Test_Error_8":Test_Error_8
                    # "Test_Error_9":Test_Error_9,
                    # "Test_Error_10":Test_Error_10
                    })

csv_name = f'Train_Test_Errors_{k}_Fold_all_Algorithm.csv'
df2.to_csv(f'CSVs_for_Plotting/{csv_name}')


